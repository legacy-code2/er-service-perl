use Mojolicious::Lite -signatures;

my $patients_in_transport = [
    {
        transport_id => 1,
        name         => 'John Doe',
        priority     => 'YELLOW',
    }
];

post '/ack' => sub($c) {
    $c->render(text => "acknowledged", status => 200);
};

post '/arrived' => sub($c) {
    $c->render(text => "OK", status => 200);
};

get '/inbound' => sub($c) {
    my $xml = "<Inbound>\n";
    foreach my $patient (@$patients_in_transport) {
        $xml .= "\t<Patient>\n";
        $xml .= "\t\t<TransportId>$patient->{transport_id}</TransportId>\n";
        $xml .= "\t\t<Name>$patient->{name}</Name>\n";
        $xml .= "\t\t<Condition>heart arrhythmia</Condition>\n";
        $xml .= "\t\t<Priority>$patient->{priority}</Priority>\n";
        $xml .= "\t\t<Birthdate></Birthdate>\n";
        $xml .= "\t</Patient>\n";
    }
    $xml .= "</Inbound>";

    $c->res->headers->content_type('application/xml');
    $c->render(text => $xml, status => 200, );
};

app->start;