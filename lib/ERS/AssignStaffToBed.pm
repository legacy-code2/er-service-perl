package ERS::AssignStaffToBed;
use strict;
use warnings FATAL => 'all';

use base 'ERS::Core';

sub assign_staff_to_bed{
    my ($self, $staff_ids, $bed_id) = @_;
    
    use Data::Dumper;
    warn Dumper [$staff_ids, $bed_id];
    
    for my $staff_id (@$staff_ids) {
        my $staff = $self->{staff_manager}->get_staff_by_id($staff_id);
        if (defined($staff)) {
            my $bed = $self->{staff_manager}->get_bed_by_id($bed_id);
            $self->{staff_manager}->assign_staff_member_to_bed($staff, $bed);
            return;
        }
    }
    die "can't find available staff\n";
}

1;