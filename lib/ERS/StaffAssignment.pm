package ERS::StaffAssignment;

use strict;
use warnings;

use base 'ERS::Core';

use ERS::Storage::Staff;
use ERS::Storage::Beds;
use ERS::Bed;
use ERS::Patient;


my $SHIFT_STAFF = [];
my $BEDS = [];
my %BED_STAFF_ASSIGNMENTS = ();

sub init {
    my $self = shift;
    $BEDS = ERS::Storage::Beds->get_all_beds();
}

sub get_shift_staff {
    if (!@$SHIFT_STAFF) {
        $SHIFT_STAFF = ERS::Storage::Staff->get_staff();
    }
    return $SHIFT_STAFF;
}

sub get_available_staff {
    my $self = shift;
    my @available_staff = ();
    for my $staff (@{$self->get_shift_staff()}) {
        my $staff_assigned = 0;
        for my $bed_list (keys %BED_STAFF_ASSIGNMENTS) {
            if (grep /$bed_list/, @$staff) {
                $staff_assigned = 1;
            }
        }
        unless ($staff_assigned) {
            push @available_staff, $staff;
        }
    }
    return \@available_staff;
}

sub get_bed_by_id{
    my ($bed) = grep $_->{id} eq $_[1], @$BEDS;
    return $bed
}

sub get_beds {
    return $BEDS;
}

sub get_staff_by_id {
    my ($self, $staff_id) = @_;

    for my $staff (@{$self->get_available_staff}) {
        return $staff
            if $staff->{id} eq $staff_id;
    }
    return undef;
}
sub get_available_beds {
    my $self = shift;
    my @available_beds = ();
    for my $bed (@{$self->get_beds()}) {
        unless ($bed->get_patient_assigned()) {
            push @available_beds, $bed;
        }
    }
    return \@available_beds;
}

sub assign_patient_to_bed {
    my ($self, $patient, $bed_id) = @_;
    my $available_beds  = $self->get_available_beds();
    my ($assigned) = grep $_->{id} == $bed_id, @$available_beds;


    my $p = ERS::Patient->new(
        $patient->{name}, $patient->{birthdate},
        $patient->{priority}, $patient->{transport_id}
    );

    $assigned->assign_patient($p);
    $BED_STAFF_ASSIGNMENTS{$assigned} = [];
}

sub assign_staff_member_to_bed {
    my ($self, $staff, $bed) = @_;

    my $currently_assigned_to_bed = [];
    for my $assigned_bed (keys %BED_STAFF_ASSIGNMENTS) {
        if ( $assigned_bed->{id} eq $bed->{id} ) {
            $currently_assigned_to_bed = $BED_STAFF_ASSIGNMENTS{$assigned_bed};
            $BED_STAFF_ASSIGNMENTS{$assigned_bed} = $staff;
        }
    }
}

1;