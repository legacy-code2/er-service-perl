package ERS::Storage::Staff;

use strict;
use warnings;

my $STAFF;

sub load {
    my ($class, $file) = @_;
    my $staff_list = [];
    open(my $fh, "<", $file) || die "can't open file $file: $@";
    while (my $line = <$fh>) {
        my ($id, $name, $role) = split /,/, $line;
        $role =~ s/\s+$//;
        push @$staff_list, {
            id   => $id,
            name => $name,
            role => $role
        };
    }
    warn "loaded staff: ".@$staff_list;
    $STAFF = $staff_list
}

sub get_staff {
    die "staff should be loaded!!!"
        unless $STAFF;
    return $STAFF;
}

1;