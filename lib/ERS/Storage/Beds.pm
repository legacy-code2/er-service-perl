package ERS::Storage::Beds;

use strict;
use warnings FATAL => 'all';

use ERS::Bed;

sub get_all_beds {
    my $class = shift;
    my $file  = shift // "data/beds.csv";

    my $beds = [];
    open(my $fh, "<", $file) || die "can't open file $file: $@";
    while (my $line = <$fh>) {
        $line =~ s/\s+$//;
        my ($id, $equipped_crit) = split /,/, $line;
        push @$beds, ERS::Bed->new($id, $equipped_crit eq 'true' ? 1 :0 );
    }
    return $beds;
}

1;