package ERS::Patient;
use strict;
use warnings FATAL => 'all';

sub new {
    my ($class, $name, $birthdate, $priority, $transport_id) = @_;

    return bless {
        _name => $name,
        _birthdate => $birthdate,
        _priority => $priority,
        _transport_id => $transport_id
    }, $class;
}

sub get_name {
    return shift->{_name};
}

sub get_birthdate {
    return shift->{_birthdate};
}

sub get_priority {
    return shift->{_priority};
}

sub get_transport_id {
    return shift->{_transport_id};
}

1;