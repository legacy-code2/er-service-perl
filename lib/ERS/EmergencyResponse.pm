package ERS::EmergencyResponse;

use strict;
use warnings;

use base 'ERS::Core';

use LWP::UserAgent;

sub form_url {
    my ($self, $path) = @_;

    if ($path =~/\//) {
        $path = substr($path, 1);
    }

    return "$self->{addr}:$self->{port}/$path";
}


sub request_inbound_diversion {
    my ($self, $priority) = @_;
    my $response;
    my $ua = LWP::UserAgent->new(timeout => $self->{timeout});
    eval {
        $response = $ua->post($self->form_url("/diversion?priority=" + $priority ));
    };

    if ($response->is_success) {
        warn("Unable to inform of diversion")
    }

    if ($response->decoded_content ne 'OK') {
        warn("Unable to inform of diversion")
    }
}

sub remove_inbound_diversion {
    my ($self, $priority) = @_;
    my $response;
    my $ua = LWP::UserAgent->new(timeout => $self->{timeout});
    eval {
        $response = $ua->post($self->form_url("/diversionStop?priority=" + $priority ));
    };

    if ($response->is_success) {
        warn("Unable to inform of diversion")
    }

    if ($response->decoded_content ne 'OK') {
        warn("Unable to inform of diversion")
    }
}
1;