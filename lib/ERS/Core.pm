package ERS::Core;

use strict;
use warnings;

use CPAN::Meta::YAML;
use Carp;

sub new {
    my ($package, %args) = @_;
    my $ref =  bless \%args, $package;

    $ref->init($args{config});

    return $ref;
}

sub init {
    my $self = shift;
    my $config = shift;
    return
        if defined($config);
    $self->load_config($ENV{CONFIG_FILE})
}

sub load_config {
    my $self = shift;
    my $file = shift || croak 'CONFIG_FILE is not set!';
    warn "used config: $file";
    open my $fh, "<:utf8", $file;
    my $yaml_text = do { local $/; <$fh> };
    my $yaml = CPAN::Meta::YAML->read_string($yaml_text)
        or die CPAN::Meta::YAML->errstr;
    $self->{config} = $yaml->[0];
}

1;