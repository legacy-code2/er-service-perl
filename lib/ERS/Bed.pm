package ERS::Bed;
use strict;
use warnings FATAL => 'all';

sub new {
    my $ref = shift;
    my ($id, $critical_care) = @_;

    return bless {
        id            => $id,
        critical_care => $critical_care
    }, $ref;
}

sub assign_patient {
    my ($self, $patient) = @_;
    $self->{patient_assigned} = $patient;
}

sub patient_discharged {
    delete shift->{patient_assigned};
}

sub get_patient_assigned {
    return shift->{patient_assigned};
}

1;