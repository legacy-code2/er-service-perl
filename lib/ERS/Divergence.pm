package ERS::Divergence;

use strict;
use warnings;

use base 'ERS::Core';

use constant FALSE => 0;
use constant TRUE => 1;

sub init {
    my $self = shift;

    $self->SUPER::init();

    my %defaults = (
        red_divergence    => FALSE,
        yellow_divergence => FALSE,
        green_divergence  => FALSE,
        red_count         => 0,
        yellow_count      => 0,
        green_count       => 0,
        allowed_count     => 3,
        red_over          => 0,
        yellow_over       => 1,
        green_over        => 4,
    );

    $self->{divergence} = \%defaults
}

sub check {
    my $self = shift;
    my $staffAssignment = ERS::StaffAssignment->new();
    my $transport = ERS::EmergencyResponse->new(addr => "http://ers.service.local", port => 4567, timeout =>1000);
    my $inbound = new ERS::InboundPatients;
    my $red = [ 1, 2 ];
    my $yellow = [ 1, 1 ];
    my $green = [ 0, 1 ];
    my $red_incremented = FALSE;
    my $yellow_incremented = FALSE;
    my $green_incremented = FALSE;
    my $patients = $inbound->get_current_inbound_patients();
    my $staff = $staffAssignment->get_available_staff();
    my $beds = $staffAssignment->get_available_beds();
    my $bedcrits = 0;
    my $redin = 0;
    my $yellowin = 0;
    my $greenin = 0;
    my @staffcur = (0, 0);
    my @need = (0, 0);

    for my $bed (@$beds) {
        $bedcrits += 1 if $bed->{critical_care}
    }

    for my $patient (@$patients) {
        if ($patient->{priority} eq 'RED') {
            $redin += 1;
        } elsif ($patient->{priority} eq 'YELLOW') {
            $yellowin += 1;
        } elsif ($patient->{priority} eq 'GREEN') {
            $greenin += 1;
        }
    }
    # doctor or not
    for my $cur (@$staff) {
        $staffcur[$cur->{role} eq 'DOCTOR' ? 0 : 1]++;
    }

    if ($redin > ($bedcrits + $self->{divergence}{red_over})) {
        $self->{divergence}{red_count} += 1;
        $red_incremented = TRUE
    }
    if ($yellowin + $greenin
        > ($beds->get_size() - $bedcrits + $self->{divergence}{yellow_over} + $self->{divergence}{green_over})) {
        if ($greenin > ($beds->get_size() - $bedcrits + $self->{divergence}{green_over})
            && $yellowin <= ($beds->get_size() - $bedcrits + $self->{divergence}{yellow_over})) {
            $self->{divergence}{green_count} += 1;
            $green_incremented = TRUE;
        }
        else {
            $self->{divergence}{green_count} += 1;
            $self->{divergence}{yellow_count} += 1;
            $green_incremented = TRUE;
            $yellow_incremented = TRUE;
        }
    }

    $need[0] = $redin * $red->[0];
    $need[0] += $yellowin * $yellow->[0];
    $need[0] += $greenin * $green->[0];
    $need[1] = $redin * $red->[1];
    $need[1] += $yellowin * $yellow->[1];
    $need[1] += $greenin * $green->[1];

    if ($need[0] > $staffcur[0]) {
        my $diff = $need[0] - $staffcur[0];
        if ($greenin * $green->[0] >= $diff) {
            if (!$green_incremented) {
                $green_incremented = TRUE;
                $self->{divergence}{green_count} += 1
            }
        }
        else {
            my $both = ($yellowin * $yellow->[0]) + ($greenin * $green->[0]);
            if ($both >= $diff) {
                if (!$green_incremented) {
                    $green_incremented = TRUE;
                    $self->{divergence}{green_count} += 1
                }
                if (!$yellow_incremented) {
                    $yellow_incremented = TRUE;
                    $self->{divergence}{yellow_count} += 1
                }
            }
            else {
                if (!$green_incremented) {
                    $green_incremented = TRUE;
                    $self->{divergence}{green_count} += 1
                }
                if (!$yellow_incremented) {
                    $yellow_incremented = TRUE;
                    $self->{divergence}{yellow_count} += 1
                }
                if (!$red_incremented) {
                    $red_incremented = TRUE;
                    $self->{divergence}{red_count} += 1
                }

            }
        }
    }

    if ($need[1] > $staffcur[1]) {
        my $diff = $need[1] - $staffcur[1];
        if ($greenin * $green->[1] >= $diff) {
            if (!$green_incremented) {
                $green_incremented = TRUE;
                $self->{divergence}{green_count} += 1
            }
        }
        else {
            my $both = ($yellowin * $yellow->[1]) + ($greenin * $green->[1]);
            if ($both >= $diff) {
                if (!$green_incremented) {
                    $green_incremented = TRUE;
                    $self->{divergence}{green_count} += 1
                }
                if (!$yellow_incremented) {
                    $yellow_incremented = TRUE;
                    $self->{divergence}{yellow_count} += 1
                }
            }
            else {
                if (!$green_incremented) {
                    $green_incremented = TRUE;
                    $self->{divergence}{green_count} += 1
                }
                if (!$yellow_incremented) {
                    $yellow_incremented = TRUE;
                    $self->{divergence}{yellow_count} += 1
                }
                if (!$red_incremented) {
                    $red_incremented = TRUE;
                    $self->{divergence}{red_count} += 1
                }

            }
        }
    }

    if ($red_incremented) {
        if ($self->{divergence}{red_count} > $self->{divergence}{allowed_count} && !$self->{divergence}{red_divergence}) {
            $self->{divergence}{red_divergence} = TRUE;
            $transport->request_inbound_diversion('RED');
            $self->send_divergence_page("Entered divergence for RED priority patients!", TRUE);
            $self->{divergence}{red_count} = 0;
        }
    }
    else {
        $self->{divergence}{red_count} = 0;
        if ($self->{divergence}{red_divergence}) {
            $transport->remove_inbound_diversion('RED');
            $self->send_divergence_page("Ended divergence for RED priority patients.", TRUE);
            $self->{divergence}{red_divergence} = FALSE;
        }
    }

    if ($yellow_incremented) {
        if ($self->{divergence}{yellow_count} > $self->{divergence}{allowed_count} && !$self->{divergence}{yellow_divergence}) {
            $self->{divergence}{yellow_divergence} = TRUE;
            $transport->request_inbound_diversion('YELLOW');
            $self->send_divergence_page("Entered divergence for YELLOW priority patients!", TRUE);
            $self->{divergence}{yellow_count} = 0
        }
    }
    else {
        $self->{divergence}{yellow_count} = 0;
        if ($self->{divergence}{yellow_divergence}) {
            $transport->remove_inbound_diversion('YELLOW');
            $self->send_divergence_page("Ended divergence for YELLOW priority patients.", FALSE);
            $self->{divergence}{yellow_divergence} = FALSE;
        }
    }

    if ($green_incremented) {
        if ($self->{divergence}{green_count} > $self->{divergence}{allowed_count} && $self->{divergence}{green_divergence}) {
            $self->{divergence}{green_divergence} = TRUE;
            $transport->request_inbound_diversion('GREEN');
            $self->send_divergence_page("Entered divergence for GREEN priority patients!", TRUE);
            $self->{divergence}{green_count} = 0;
        }
    }
    else {
        $self->{divergence}{green_count} = 0;
        if ($self->{divergence}{green_divergence}) {
            $transport->remove_inbound_diversion('GREEN');
            $self->send_divergence_page("Ended divergence for GREEN priority patients.", FALSE);
            $self->{divergence}{green_divergence} = FALSE;
        }
    }
}

sub send_divergence_page{
    my ($self, $text, $require_ack) = @_;

    eval {
        my $transport = Vendor::PagerSystem->get_transport();
        $transport->initialize();
        if ($require_ack) {
            $transport->transmitRequiringAcknowledgement($self->{config}{pager_system}{ADMIN_ON_CALL_DEVICE}, $text);
        } else {
            $transport->transmit($self->{config}{pager_system}{ADMIN_ON_CALL_DEVICE}, $text);
        }
    };

    if ($@) {
       warn "ERROR: $@\n";
    }
}

1;