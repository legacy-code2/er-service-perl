package ERS::Scanner;

use strict;
use warnings;

use base 'ERS::Core';

use ERS::InboundPatients;
use Vendor::PagerSystem;

my %CRITICAL_PATIENT_NOTIFICATIONS_SENT = ();

sub scan {
    my $self = shift;
    warn "Scanning for situations requiring alerting...\n";
    my $inbound = ERS::InboundPatients->get_current_inbound_patients($self->{config});
    for my $patient (@$inbound) {
        if ($patient->{priority} eq 'RED') {
            my $key = "$patient->{name}:$patient->{transportId}";
            if (!$CRITICAL_PATIENT_NOTIFICATIONS_SENT{$key}) {
                $self->alert_for_new_critical_patient($patient);
            }
        }
    }

}

sub alert_for_new_critical_patient {
    my ($self, $patient) = @_;
    eval {
        my $transport = Vendor::PagerSystem->get_transport();
        $transport->initialize();
        $transport->transmit_requiring_acknowledgement($self->{config}{pager_system}{ADMIN_ON_CALL_DEVICE},
            "New inbound critical patient: $patient->{transportId}");
    };

    if ($@) {
        warn "ERROR: couldn't send alert: $@";
    } else {
        my $key = "$patient->{name}:$patient->{transportId}";
        $CRITICAL_PATIENT_NOTIFICATIONS_SENT{$key} = 1;
        print "ALERT SENT.\n";
    }
}

1;