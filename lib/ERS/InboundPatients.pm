package ERS::InboundPatients;

use strict;
use warnings;

use Mojo::UserAgent;

use base 'ERS::Core';

sub get_current_inbound_patients {
    my $self = shift;
    my $config = $self->{config};
    my $url = "http://$config->{emergency_response_service}{host}:$config->{emergency_response_service}{port}/inbound";
    my $ua = Mojo::UserAgent->new;
    warn "request url: $url";
    $ua->connect_timeout($config->{emergency_response_service}{timeout});
    my $res = $ua->get($url)->result;
    if ($res->code != 200) {
        warn "ERROR: " . $res->code;
    }
    warn("Received XML from transport service: " . $res->body);
    my $patients = [];
    my $xml = Mojo::DOM->new($res->body);
    for my $p ($xml->find('patient')->each) {
        my $patient = {};
        for my $n ($p->children->each) {
            if ($n->tag eq 'name') {
                $patient->{name} = $n->text
            }
            if ($n->tag eq 'transportid') {
                $patient->{transportId} = int($n->text)
            }
            if ($n->tag eq 'priority') {
                $patient->{priority} = $n->text
            }
            if ($n->tag eq 'birthday' && $n->text ne '') {
                $patient->{birthday} = $n->text
            }
            if ($n->tag eq 'condition' && $n->text ne '') {
                $patient->{condition} = $n->text
            }
        }
        push @$patients, $patient
    }
    return $patients;
}

sub inform_of_patient_arrival {
    my ($self, $transport_id) = @_;

    my $config = $self->{config};
    my $url = "http://$config->{emergency_response_service}{host}:$config->{emergency_response_service}{port}" .
        "/arrived?transportId=$transport_id";
    my $ua = Mojo::UserAgent->new;
    warn "request url: $url";
    $ua->connect_timeout($config->{emergency_response_service}{timeout});
    my $result = $ua->post($url)->result;

    if ($result->body() ne 'OK') {
        die "Unable to inform of arrival\n";
    }
}

1;