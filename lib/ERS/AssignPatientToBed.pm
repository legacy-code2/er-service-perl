package ERS::AssignPatientToBed;
use strict;
use warnings FATAL => 'all';

use base 'ERS::Core';

sub assign_patient_to_bed {
    my ($self, $transportId, $bed_id) = @_;
    my $bed = $self->{staff_manager}->get_bed_by_id($bed_id);
    my $patient = $self->get_patient_by_transport($transportId);
    $self->{staff_manager}->assign_patient_to_bed($patient, $bed_id);
    eval {
        $self->{inbound_patient}->inform_of_patient_arrival($transportId);
    };

    if ($@) {
        die "some error happen: $@";
    }
}

sub get_patient_by_transport {
    my ($self, $transportId) = @_;

    my $inbounds = $self->{inbound_patient}->get_current_inbound_patients();

    for my $patient (@$inbounds) {
        return $patient
            if $patient->{transportId} == $transportId;

    }

    die "Unable to find inbound patient " + $transportId;
}


1;