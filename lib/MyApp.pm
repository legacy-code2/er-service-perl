package MyApp;
use Mojo::Base 'Mojolicious', -signatures;
use ERS::Storage::Staff;
use ERS::StaffAssignment;

sub startup ($self) {

  # Load configuration from config file
  my $config = $self->plugin('NotYAMLConfig');

  # Configure the application
  $self->secrets($config->{secrets});

  ERS::Storage::Staff->load($config->{storage}{staff});
  ERS::StaffAssignment->init();

  my $connection_string = "http://$config->{emergency_response_service}{host}:$config->{emergency_response_service}{port}";
  warn ("Initializing with Emergency Response Services: $connection_string/ack");
  my $result = $self->ua->post($connection_string . "/ack", "ER system ping");

  if ($result->res->code != 200 || $result->res->body ne 'acknowledged') {
    die ("Unable to connect with Emergency Respose Services code: ".$result->res->code);
  }

  # Router
  my $r = $self->routes;
  $r->get('/')->to('Root#info');
  $r->get('/api/inboundPatients')->to('InboundPatients#current');
  $r->get('/api//shiftStaff')->to('Staff#shift');
  $r->get('/api/availableStaff')->to('Staff#available');
  $r->get('/api/physiciansOnDuty')->to('PhysiciansOnDuty#get');
  $r->get('/api/beds')->to('Beds#get_all');
  $r->get('/api/availableBeds')->to('Beds#available');
  $r->post('/api/assignPatientToBed')->to('Beds#assignPatient');
  $r->post('/api/assignStaffToBed')->to('Beds#assignStaff');
}

1;
