package MyApp::Controller::PhysiciansOnDuty;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use Mojo::DOM;

use ERS::StaffAssignment;

sub get($self) {
    warn "Recieved request for physicians on duty from client.";
    my $physicians = [];

    for my $staff (@{ERS::StaffAssignment->get_shift_staff()}) {
        push @$physicians, $staff
            if $staff->{role} eq 'DOCTOR';
    }
    return $self->render(json => $physicians, status => 200);
}

1;