package MyApp::Controller::Staff;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use ERS::StaffAssignment;

sub shift($self) {
    warn "Received request for all shift staff from client.";
    my $physicians = ERS::StaffAssignment->get_shift_staff();
    return $self->render(json => $physicians, status => 200);
}

sub available($self) {
    warn "Received request for available staff from client.";
    my $physicians = ERS::StaffAssignment->get_available_staff();
    return $self->render(json => $physicians, status => 200);
}

1;