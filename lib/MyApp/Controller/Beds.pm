package MyApp::Controller::Beds;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use ERS::StaffAssignment;
use ERS::AssignPatientToBed;
use ERS::AssignStaffToBed;

sub get_all($self) {
    warn "Received request for all beds from client.";
    my $beds = ERS::StaffAssignment->get_beds();

    my @result = ();
    foreach my $bed (@$beds) {
        my $out = {
            bedId           => $bed->{id} + 0,
            criticalCare => \$bed->{critical_care},
        };
        if ($bed && $bed->get_patient_assigned()) {
            my $patient = $bed->get_patient_assigned();
            $out->{patientAssigned} = {
                name        => $patient->get_name(),
                priority    => $patient->get_priority(),
                transportId => $patient->get_transport_id(),
            };
            $out->{patientAssigned}{birthdate} = $patient->get_birthdate()
                if $patient->get_birthdate() && $patient->get_birthdate() ne '';
        } else {
            $out->{patientAssigned} = undef;
        }
        push @result, $out
    }

    return $self->render(json => \@result, status => 200);
}

sub available($self) {
    warn "Received request for available beds from client.";
    my $beds = ERS::StaffAssignment->get_available_beds();

    my @result = ();
    foreach my $bed (@$beds) {
        push @result, {
            id              => $bed->{id} + 0,
            criticalCare    => \$bed->{critical_care},
            patientAssigned => $bed->{patient_assigned},
            staff           => $bed->{staff},
        }
    }

    return $self->render(json => \@result, status => 200);
}

sub assignPatient($self) {
    my $transportId = $self->param("transportId");
    my $bed_id = $self->param("bedId");
    warn "Client request to assign patient $transportId to bed $bed_id";
    my $config = $self->app->plugin('NotYAMLConfig');

    my $inbounds = ERS::InboundPatients->new(config => $config);
    my $command = ERS::AssignPatientToBed->new(
        config          => $config,
        staff_manager   => ERS::StaffAssignment->new(),
        inbound_patient => $inbounds,
    );

    eval {
        $command->assign_patient_to_bed($transportId, $bed_id);
    };

    if ($@) {
        return $self->render(text => "ERROR: $@", status => 400);
    }
    else {
        return $self->render(text => 'OK', status => 200);
    }

}

sub assignStaff($self) {
    my $params = $self->req->params->to_hash;
    warn "Client request to assign patient $params->{staffId} to bed $params->{bedId}";
    my $command = ERS::AssignStaffToBed->new(
        config        => $self->app->plugin('NotYAMLConfig'),
        staff_manager => ERS::StaffAssignment->new());
    eval {
        $command->assign_staff_to_bed($params->{staffId}, $params->{bedId});
    };

    if ($@) {
        return $self->render(text => "ERROR: $@", status => 400);
    }
    else {
        return $self->render(text => 'OK', status => 200);
    }
}


1;