package MyApp::Controller::InboundPatients;

use Mojo::Base 'Mojolicious::Controller', -signatures;
use ERS::InboundPatients;

sub current($self) {
    warn "Recieved request for inbound patients from client.";
    my $config = $self->app->plugin('NotYAMLConfig');

    die("can't read config")
        unless defined( $config);

    my $p = ERS::InboundPatients->new(config => $config);
    my $patients = $p->get_current_inbound_patients();
    warn("Returning inbound patients: " . @$patients);
    if (@$patients > 0) {
        $self->render(json => $patients, status => 200);
    } else {
        $self->render(json => [], status => 404);
    }
}


1;