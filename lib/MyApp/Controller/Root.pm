package MyApp::Controller::Root;

use Mojo::Base 'Mojolicious::Controller', -signatures;

sub info($self) {

    my $text = "<h1>Available endpoints:</h1>
        GET /inboundPatients<br />
        GET /shiftStaff<br />
        GET /availableStaff<br />
        GET /physiciansOnDuty<br />
        GET /beds<br />
        GET /availableBeds<br />
        POST /assignPatientToBed<br />
        POST /assignStaffToBed<br />";

    return $self->render(text => $text, status => 200);
}

1;