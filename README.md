# er-service-perl


## Docker

### Build image
```
docker build -t er-server-perl .
```

### run interactive shell
```
docker run -v "$(pwd):/usr/src/app" -it er-server-perl /bin/bash
```

### run service
```bash
docker run -v "$(pwd):/usr/src/app" -p 3000:3000 -it er-server-perl /usr/local/bin/morbo /usr/src/app/script/er_server
```

### run fake server
```shell
perl script/fake_response_server.pl daemon -l http://*:4567
```